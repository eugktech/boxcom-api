<?php

namespace Eugktech\Box\Exceptions;

use Exception;
use Psr\Http\Message\ResponseInterface;

class BadRequest extends Exception
{
    public ResponseInterface $response;

    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;

        $body = json_decode($response->getBody(), true);

        parent::__construct( ($body !== null) ? $body['message'] : null, $response->getStatusCode());
    }
}
