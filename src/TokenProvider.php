<?php

namespace Eugktech\Box;

interface TokenProvider
{
    public function getToken(): string;
}

