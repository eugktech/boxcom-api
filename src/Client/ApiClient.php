<?php

namespace Eugktech\Box\Client;

class ApiClient extends AbstractClient
{
    const URI = 'https://api.box.com';
    const API_VERSION = '2.0';
}
