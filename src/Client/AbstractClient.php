<?php

namespace Eugktech\Box\Client;

use GuzzleHttp\Client as GuzzleClient;

abstract class AbstractClient extends GuzzleClient
{
    protected $accessToken;

    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;

        parent::__construct([
            'base_uri' => static::URI . '/'.static::API_VERSION.'/',
            'headers' => ['Authorization' => 'Bearer ' . $accessToken],
        ]);
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }
}
