<?php

namespace Eugktech\Box\Client;

use GuzzleHttp\Client as GuzzleClient;

class OauthClient extends GuzzleClient
{
    const URI = 'https://api.box.com';

    public function __construct()
    {
        parent::__construct([
            'base_uri' => static::URI . '/',
        ]);
    }
}
