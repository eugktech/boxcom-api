<?php

namespace Eugktech\Box\Client;

class UploadClient extends AbstractClient
{
    const URI = 'https://upload.box.com/api';
    const API_VERSION = '2.0';
}
