<?php

namespace Eugktech\Box;

use Eugktech\Box\Client\ApiClient;
use Eugktech\Box\Client\OauthClient;
use Eugktech\Box\Client\UploadClient;
use Eugktech\Box\Exceptions\BadRequest;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\StreamWrapper;
use Psr\Http\Message\ResponseInterface;
use Exception;

class Client
{
    protected ApiClient $client;

    protected UploadClient $uploadClient;

    protected OauthClient $oauthClient;

    public function __construct(
        $accessTokenOrAppCredentials = null,
    ) {
        if (is_array($accessTokenOrAppCredentials)) {
            $this->oauthClient = new OauthClient();
            $token = $this->requestAccessToken($accessTokenOrAppCredentials);
        }
        if (is_string($accessTokenOrAppCredentials)) {
            $token = (new InMemoryTokenProvider($accessTokenOrAppCredentials))->getToken();
        }
        $this->client = new ApiClient($token);
        $this->uploadClient = new UploadClient($token);
    }

    public function requestAccessToken(array $credentials): mixed
    {
        if(empty($credentials))
        {
            return false;
        }
        $parameters = [];
        $parameters['client_id'] = $credentials['client_id'] ?? null;
        $parameters['client_secret'] = $credentials['client_secret'] ?? null;
        $parameters['grant_type'] = $credentials['grant_type'] ?? 'client_credentials';
        $parameters['box_subject_type'] = $credentials['box_subject_type'] ?? 'enterprise';
        $parameters['box_subject_id'] = $credentials['box_subject_id'] ?? null;

        return $this->oauthEndpointRequest("oauth2/token", 'post', $parameters);
    }

    public function getFileInformation(int $id, array $parameters = []): array
    {
        return $this->rpcEndpointRequest("files/{$id}", 'get', $parameters);
    }

    public function getFolderInformation(int $id, array $parameters = []): array
    {
        return $this->rpcEndpointRequest("folders/{$id}", 'get', $parameters);
    }

    /**
     * @param int $id
     * @param array $parameters
     * @return resource
     * @throws Exception
     */
    public function download(int $id, array $parameters = [])
    {
        $response = $this->downloadEndpointRequest("files/{$id}/content", 'get', $parameters);

        return StreamWrapper::getResource($response->getBody());
    }

    public function upload(string $fileName, int $parentFolderId, $contents): ResponseInterface
    {
        $attributes = [
            'name' => $fileName,
            'parent' => ['id' => $parentFolderId],
        ];

        return $this->uploadEndpointRequest("files/content", 'post', $attributes, $contents);
    }

    public function listItemsInFolder(int $folderId): array
    {
        return $this->rpcEndpointRequest("folders/{$folderId}/items", 'get');
    }

    public function delete(int $fileId): array
    {
        return $this->rpcEndpointRequest("files/{$fileId}", 'delete');
    }

    public function deleteFolder(int $folderId): array
    {
        return $this->rpcEndpointRequest("folders/{$folderId}", 'delete');
    }

    public function createFolder(string $folderName, int $parentFolderId = 0): array
    {
        $parameters = [
            'name' => $folderName,
            'parent' => ['id' => $parentFolderId],
        ];

        return $this->rpcEndpointRequest("folders", 'post', $parameters);
    }

    protected function oauthEndpointRequest(string $endpoint, string $method = 'post', array $parameters = []): mixed
    {
        $options = [];
        if ($parameters) {
            $options['json'] = $parameters;
        }

        try {
            $response = $this->oauthClient->$method($endpoint, $options);
        } catch (ClientException $exception) {
            throw $this->determineException($exception);
        }

        $response = json_decode($response->getBody(), true);

        return $response['access_token'] ?? null;
    }

    protected function rpcEndpointRequest(string $endpoint, string $method = 'post', array $parameters = []): mixed
    {
        $options = [];
        if ($parameters) {
            $options['json'] = $parameters;
        }

        try {
            $response = $this->client->$method($endpoint, $options);
        } catch (ClientException $exception) {
            throw $this->determineException($exception);
        }

        $response = json_decode($response->getBody(), true);

        return $response ?? [];
    }

    protected function downloadEndpointRequest(string $endpoint, string $method = 'post', array $parameters = []): ResponseInterface
    {
        $options = [];
        if ($parameters) {
            $options['json'] = $parameters;
        }

        try {

            $response = $this->client->$method($endpoint, $options);
        } catch (ClientException $exception) {
            throw $this->determineException($exception);
        }

        return $response;
    }

    protected function uploadEndpointRequest(string $endpoint, string $method, array $arguments, $fileContent): ResponseInterface
    {
        $options = [];
        $multipart = [];
        $fileName = '';

        foreach ($arguments as $k => $v) {
            if ($k === 'name') {
                $fileName = $v;
            } elseif ($k === 'parent') {
                $k = 'parent.id';
                $v = $v['id'];
            }
            $multipart[] = ['name' => $k, 'contents' => $v];
        }

        $multipart[] = [
            'Content-type' => 'multipart/form-data',
            'name' => 'file',
            'contents' => $fileContent,
            'filename' => $fileName
        ];

        $options['multipart'] = $multipart;

        try {
            $response = $this->uploadClient->$method($endpoint, $options);
        } catch (ClientException $exception) {
            throw $this->determineException($exception);
        }

        return $response;
    }

    protected function determineException(ClientException $exception): Exception
    {
        if (in_array($exception->getResponse()->getStatusCode(), [400, 409])) {
            return new BadRequest($exception->getResponse());
        }

        return $exception;
    }
}
